import {getTransporter} from "../core/nodemailer";

import { adminEmail, emailConfig } from "../config";

export default async data => {
    const transporter = await getTransporter();
    const mailData = { ...data, from: emailConfig.username, to: data.to || adminEmail };
    await transporter.sendMail(mailData, async function (err, info) {
        if (err)
            console.log('error', err)
        else
            console.log('success', info);
    });
    return true;
}