export const CHANGE_COMMON_DATA = 'CHANGE_COMMON_DATA';

export const CHANGE_USER_DATA = 'CHANGE_USER_DATA';

export const OPEN_MODAL = "OPEN_MODAL";
export const CLOSE_MODAL = "CLOSE_MODAL";

export const SET_USER = "SET_USER";
