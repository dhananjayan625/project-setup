import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router';
import { renderRoutes } from 'react-router-config';
import { Provider } from 'react-redux';
import minifyCssString from 'minify-css-string'
import StyleContext from 'isomorphic-style-loader/StyleContext';

import Routes from '../Routes';

export default ({ req, store, title }) => {
  const preloadedState = store.getState();
  let titles = title ? `${title} | React SSR` : "Page not found | React SSR"
  if (process.env.NODE_ENV === 'development') {
    return (`
			<!doctype html>
			<html>
				<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- Icons -->
        <link rel="icon" href="/images/taxi_ico.ico" type="image/icon">
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
					<title>${titles}</title>
          <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
          <script>
          // WARNING: See the following for security issues around embedding JSON in HTML:
          // https://redux.js.org/usage/server-rendering#security-considerations
          window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(
      /</g,
      '\\u003c'
    )}
        </script>
				</head>
				<body>
					<div id='root'></div>
					<script src='/dist/bundle.js'></script>
				</body>
			</html>
		`);
  } else if (process.env.NODE_ENV === 'production') {
    const css = new Set() // CSS for all rendered React components
    const insertCss = (...styles) => styles.forEach(style => css.add(style._getCss()))
    const content = renderToString(
      <Provider store={store}>
        <StyleContext.Provider value={{ insertCss }}>
          <StaticRouter location={req.path} context={{}}>
            <div>{renderRoutes(Routes)}</div>
          </StaticRouter>
        </StyleContext.Provider>
      </Provider>,
    );
    return (`
    <!doctype html>
    <html>
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Icons -->
    <link rel="icon" href="/images/taxi_ico.ico" type="image/icon">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <title>${titles}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>${minifyCssString([...css].join(''))}</style>
    </head>
      <body>
        <div id="root">${content}</div>
        <script>
          // WARNING: See the following for security issues around embedding JSON in HTML:
          // https://redux.js.org/usage/server-rendering#security-considerations
          window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}
        </script>
        <script src="/dist/bundle.js"></script>
      </body>
    </html>
		`);
  }
};