const dotenv = require('dotenv');
const result = dotenv.config();
if (result.error) {
  throw result.error;
}
// const { parsed: envs } = result;
// console.log(envs);

export const url = 'http://localhost:5500';

export const isDevelopment = process.env.NODE_ENV === 'development';


export const dbUsername = process.env.DB_USERNAME || "dhananjayan";
export const dbPassword = process.env.DB_PASSWORD || "Root123!";
export const dbName = process.env.DB_NAME || "sample";

export const jwtSecretKey = process.env.JWT_SECRET_KEY || "YOUR_SECRET_KEY";

export const emailConfig = {
  username: process.env.SMTP_USERNAME,
  password: process.env.SMTP_PASSWORD,
  host: process.env.SMTP_HOST || "smtp.gmail.com",
  port: process.env.SMTP_PORT || "465",
  clientId: process.env.CLIENT_ID,
  clientSecret: process.env.CLIENT_SECRET,
  redirectUri: process.env.REDIRECT_URI,
  refreshToken: process.env.REFRESH_TOKEN
}