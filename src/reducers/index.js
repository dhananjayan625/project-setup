import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import { reducer as toastr } from 'react-redux-toastr';
import common from './common';

export default combineReducers({
  common,
  form,
  toastr
});
