import { FETCH_REQUEST_QUERY } from '../actions';

import { CHANGE_COMMON_DATA } from '../helpers/constants';
export default (state = {}, action) => {
  switch (action.type) {
    case FETCH_REQUEST_QUERY:
      return action.payload;

    case CHANGE_COMMON_DATA:
      return {
        ...state,
        recentBookingFormStatus: state.bookingFormStatus,
        ...action.payload,
      };
    default:
      return state;
  }
};
