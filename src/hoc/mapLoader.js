import React, { Component } from 'react';

import ReactGoogleMapLoader from "react-google-maps-loader"
import { googleMapApi } from '../helpers/constants';

export default function mapLoader(ComposedComponent) {
    return class extends Component {
        render() {
            return (
                <ReactGoogleMapLoader
                    params={{
                        key: googleMapApi, // Define your api key here
                        libraries: "places", // To request multiple libraries, separate them with a comma
                    }}
                    render={googleMaps => {
                        return <ComposedComponent {...this.props} googleMaps={googleMaps} />
                    }}
                />
                
            );
        }
    }
}