const logger = store => next => action => {
    // console.group(action.type)
    if(process.env.NODE_ENV !== 'production')console.info('dispatching', action)
    let result = next(action)
    // console.log('next state', store.getState())
    // console.groupEnd()
    return result
  }
  
  export default logger