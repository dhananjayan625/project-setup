import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import { renderRoutes } from 'react-router-config';
import ReduxToastr from 'react-redux-toastr';

import useStyles from 'isomorphic-style-loader/useStyles';

import s from './App.css';

const App = ({ route }) => {
  useStyles(s);
  return (
    <Fragment>
      <div key={'root'}>{renderRoutes(route.routes)}</div>
      <ReduxToastr
        timeOut={4000}
        newestOnTop={false}
        preventDuplicates
        position="top-right"
        getState={(state) => state.toastr} // This is the default
        transitionIn="fadeIn"
        transitionOut="fadeOut"
        closeOnToastrClick />
    </Fragment>
  )
};

App.propTypes = {
  route: PropTypes.objectOf(PropTypes.any),
};

App.defaultProps = {
  route: null,
};

export default { component: App };