import express from 'express';
import { matchRoutes } from 'react-router-config';
import cookieParser from 'cookie-parser';

import models from './data/models';
import Routes from './Routes';
import renderer from './helpers/renderer';
import createStore from './store';

require('dotenv').config();

const app = express();

const port = process.env.PORT || 5500;

app.use(express.static('dist'));
app.use(express.static('public'));
app.use(express.json());
app.use(cookieParser());

app.get('*', async (req, res) => {
  const store = createStore();
  let title;
  const routes = matchRoutes(Routes, req.path);
  const promises = routes.map(({ route }) => {
    // route.loadData ? route.loadData(dispatch, url) : null;
    if (route.title) title = route.title;
  });
  Promise.all(promises).then(() => {
    const content = renderer({ req, store, title });
    res.send(content);
  });
});

models.sync().then(function () {
  app.listen(port, () => {
    console.log(`Listening on port: ${port}`);
  });
});