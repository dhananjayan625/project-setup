import { Sequelize } from 'sequelize';
import { dbName, dbPassword, dbUsername } from '../config';
const sequelize = new Sequelize(dbName, dbUsername, dbPassword, { dialect: "mysql" });

export default sequelize;