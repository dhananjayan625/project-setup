import sequelize from '../sequelize';

// models
import User from './User';

function sync(...args) {
  return sequelize.sync(...args);
}

export default { sync }

export {
  User
};