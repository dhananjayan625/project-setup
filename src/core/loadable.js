import React from 'react';

import baseLoadable from '@loadable/component';

import Loader from '../components/Loader/Loader';

export default (func) => baseLoadable(func, { fallback: <Loader /> });

export const loadableLib = (func) => {
    return baseLoadable.lib(func)
}