import { createBrowserHistory, createMemoryHistory } from 'history';

// Navigation manager, e.g. history.push('/home')
// https://github.com/mjackson/history
// export default typeof window !== undefined && createBrowserHistory();

const isServer = typeof window === 'undefined';

export default isServer
    ? createMemoryHistory({
        initialEntries: ['/']
    })
    : createBrowserHistory();
