import { google } from 'googleapis';
import nodemailer from 'nodemailer';

import { clientId, clientSecret, redirectUri, refreshToken } from '../config';

const oAuth2Client = new google.auth.OAuth2(clientId, clientSecret, redirectUri)
oAuth2Client.setCredentials({refresh_token: refreshToken})

export  const getTransporter = async () => {
    try{
        const accessToken = await oAuth2Client.getAccessToken();
        const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                type: 'OAuth2',
                user: 'sudhamavijay@gmail.com',
                clientId,
                clientSecret,
                refreshToken,
                accessToken
            }
        });
        return transporter;
    } catch(e) {
        console.log(e)
        return false;
    }
}