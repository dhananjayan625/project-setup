import { User } from '../data/models';
import { verifyToken } from './jwt';

export const authorization = async (req, res, next) => {
  const token = req.cookies.access_token;
  if (!token) return next();
  try {
    const user = verifyToken(token);
    if (!user) return next();
    if (user.isAdmin) {
      req.user = { isAdmin: true };
      return next();
    }
    const isUserExist = await User.findOne({ where: { id: user.id, phoneNumber: user.phoneNumber }, raw: true })
    if (!isUserExist) return next();
    req.user = user;
    return next();
  } catch (e) {
    console.log(e);
    return next();
  }
};