import jwt from "jsonwebtoken";
import { jwtSecretKey } from "../../src/config";

export const createToken = data => jwt.sign(data, jwtSecretKey);

export const verifyToken = token => jwt.verify(token, jwtSecretKey);