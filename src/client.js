import React from 'react';

import { hydrate, render } from 'react-dom';
import { Router } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import { Provider } from 'react-redux';
import StyleContext from 'isomorphic-style-loader/StyleContext';

import Routes from './Routes';
import createStore from './store';
import history from './core/history';

const insertCss = (...styles) => {
  const removeCss = styles.map(style => style._insertCss());
  return () => removeCss.forEach(dispose => dispose());
}

// Grab the state from a global variable injected into the server-generated HTML
const preloadedState = window.__PRELOADED_STATE__;
const isServer = typeof window === 'undefined';
// let isMarkupPresent = document.getElementById('root').hasChildNodes();
// const renderReactApp = isServer ? render : isMarkupPresent ? hydrate : render;
const renderReactApp = isServer ? hydrate : render;

// Allow the passed state to be garbage-collected
delete window.__PRELOADED_STATE__;

// Create Redux store with initial state
const store = createStore(preloadedState);

renderReactApp(
  <Provider store={store}>
    <StyleContext.Provider value={{ insertCss }}>
      <Router history={history}>
        {renderRoutes(Routes)}
      </Router>
    </StyleContext.Provider>
  </Provider>,
  document.getElementById('root'),
);