const path = require('path');
const webpackNodeExternals = require('webpack-node-externals');
const TerserPlugin = require("terser-webpack-plugin");
const isProduction = process.env.NODE_ENV === 'production';
// const LoadablePlugin = require('@loadable/webpack-plugin')

const commonConfig = {
    devtool: 'source-map',
    mode: isProduction ? 'production' : 'development',
    watch: !isProduction,
    module: {
        rules: [
            {
                test: /.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {},
                },
            },
            {
                test: /\.css$/,
                use: [
                    'isomorphic-style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                            modules: {
                                localIdentName: '[name]__[local]--[hash:base64:5]'
                            },
                            esModule: false,
                        }
                    },
                    'postcss-loader'
                ]
            },
            {
                test: /\.(png|jpg|webp|webm|gif)$/,
                loader: 'url-loader'
            },
        ],
    },
    // plugins: [new LoadablePlugin()],
    resolve: {
        extensions: [".js", ".jsx"],
        alias: {
            Components: path.resolve(__dirname, "../src/components"),
            Constants: path.resolve(__dirname, "../src/helpers/constants"),
            Actions: path.resolve(__dirname, "../src/actions"),
            Images: path.resolve(__dirname, "../public/images"),
            Hooks: path.resolve(__dirname, "../src/hooks")
        }
    },
    optimization: {
        minimize: true,
        minimizer: !isProduction ? [] : [
            new TerserPlugin({ /* additional options here */ }),
        ],
        splitChunks: {
            chunks: 'async',
            minSize: 20000,
            minChunks: 1,
            maxAsyncRequests: 30,
            maxInitialRequests: 30,
            enforceSizeThreshold: 50000,
            cacheGroups: {
                defaultVendors: {
                    test: /[\\/]node_modules[\\/]/,
                    priority: -10,
                    reuseExistingChunk: true,
                },
                default: {
                    minChunks: 2,
                    priority: -20,
                    reuseExistingChunk: true,
                },
            },
        },
    }
}

module.exports = [{
    target: 'node',
    entry: ['@babel/polyfill', './src/server.js'],
    externals: [webpackNodeExternals()],
    output: {
        filename: 'server.js',
        chunkFilename: '[name].chunk.js',
        path: path.resolve(__dirname, '../build'),
        clean: true
    },
    ...commonConfig
},
{
    target: 'web',
    mode: 'development',
    entry: ['@babel/polyfill', './src/client.js'],
    output: {
        publicPath: '/dist/',
        filename: 'bundle.js',
        chunkFilename: '[name].chunk.js',
        path: path.resolve(__dirname, '../public', 'dist'),
        clean: true
    },
    ...commonConfig
}];